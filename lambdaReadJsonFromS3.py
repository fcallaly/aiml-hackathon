import json
import boto3
 
s3_client = boto3.client('s3')
bucket = 'aiml-hackathon-data'
summary_file = 'everyones-output/demo-images/summary.json'
 
def lambda_handler(event, context):
    s3_object = s3_client.get_object(Bucket=bucket, Key=summary_file) 
    summary = json.loads(s3_object["Body"].read().decode())
 
    print(summary)
 
    return { "dialogAction": {
                "type": "Close",
                "fulfillmentState": "Fulfilled",
                "message": {
                    "contentType": "PlainText",
                    "content": json.dumps(summary)
                }
            }}