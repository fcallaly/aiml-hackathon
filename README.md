# AI-ML Hackathon Instructions

## Outline

The overarching goal of this hackathon is to come up with interesting and creative demonstrations and uses of the Python & AWS technologies that have been discussed.
You may also make use of experience you have with other technologies that are relevant or useful. We would like to see as wide an array of approaches as possible, so think creatively.

The topics we have covered are:

1. Core ML concepts.
2. Python fundamentals and interfacing to AWS ML Services.
3. AWS Services - Rekognition, Comprehend, Lex, Lambda, S3 

You may use any AWS Services, ask your instructor if you need access to any extra services.

Your team should try to come up with a plan and then divide the work, you may choose to go in more that one direction within the team, however **make sure** you maintain communication throughout.

Instructors will be available throughout the day for help with technical issues or with ideas.

At 2pm a group of instructors will come to your breakout room for an informal review of what you've achieved. We'll be looking for you to give a 5-10min explanation of what you've done. Preferably each person on the team will speak at some point.

We don't want you to spend a huge amount of time creating slides, however you may want to put together a very small number of slides to explain your understanding of the technologies. 

There will be prizes awarded for the most creative solution, the teams shortlisted for prizes will be asked to give their 10min description of what they did to the entire group after 3pm.

Assessment will be based on the following criteria:

- Interesting and unique uses of the technologies
- Engaging demonstration
- A good description of the concepts and technologies used
- Team communication and engagment (have fun!)
- Creative ideas for future use of the technologies

## Provided Data & Tools

You have been provided with an S3 folder containing image files. There are 2 versions of this folder:

- s3://aiml-hackathon-data/image-set-full  : this is 998 images randomised
- s3://aiml-hackathon-data/image-set-small : this is a random selection of 30 from the full image set, useful for testing ideas

A simple web page has been created to make it easier to browse the images (and your results). That web page is here:

https://aiml-hackathon-ui.s3.eu-west-1.amazonaws.com/index.html

Note that the source code for the above page is a single HTML and a single Javascript file. These are in S3 at `s3://aiml-hackathon-ui`. If anyone on your team has HTML/Javascript experience, you may choose to update this, or create your own web page.

## Suggested Goals

The goals below are suggested, however your team is free to choose to emphasise some and de-emphasise others.

You may also choose completely different goals and make use of completely different datasets. This is **YOUR** opportunity to experiment and be creative.

If you want to consider taking your own unique approach, you may find useful datasets at https://www.kaggle.com/datasets

An **example** of what is meant by an alternative approach might be take a dataset such as [this one](https://www.kaggle.com/praveengovi/emotions-dataset-for-nlp?select=train.txt) and use Python to analyse it, perhaps use AWS Comprehend to analyse it, OR EVEN create your own ML model e.g. with Scikit-learn. Ask your instructors if you have experience and would like to consider an approach such as this.

An alternative approach such as this may be take by a subset of your team, while other team members continue with the goals outlined below.

### Suggested Goal 1: Gather as much useful information as you can about the image set.

Some initial suggestions as to what should be gathered are:

- How many images in total are in the folder
- How many images contain cars
- How many images contain people
- How many images contain text
- How many images contain license plates, and what is the text of the license plates
- Of the images that contain text:
    - Extract the key entities
    - Determine the sentiment of the text
- Of the images that contain people estimate:
    - gender
    - approximate age
    - emotion etc.
- Identify other objects in images - houses, streets, trees, animals etc.

If you store the gathered information in JSON format in a folder in AWS S3 it will make it easier for instructors to review. It will also be accessible by the demonstration webpage mentioned above.

It would be great to also make use of Jupyter notebooks as a presentation format for your results. You should also make good use of markdown cells for this.

You may use any suitable technologies to achieve this goal. As covered in the pre-hackathon sessions, some AWS ML Services along with some simple Python code could be used.

### Suggested Goal 2: Create a Lex chatbot that you can ask questions about the set of images.

Ideally you could ask the chatbot various questions and it will give correct answers

Some possible questions for the chatbot:

- "How many images are in the folder?"
- "Tell me what you found in image 10?"
- "What entities were found in text in the images?"
- "How many images contain people?"
- "How many images contain cars?"

Try to come up with more interesting ideas for questions that you might ask this chatbot.

**Note** You may choose to create a chatbot that does something completely different, queries other datasets or uses other AWS services.

### Suggested Goal 3: Get Creative!

Given the broad goal of creative uses of the tools and technologies discussed, try to come up with additional or even completely different ideas of your own. Ask your instructor for advice on how you might implement your ideas.

## Technical information

The results of what you find should be stored in a new folder created by your team in the ```aiml-hackathon-data``` folder in S3.

An example Jupyter notebook is included in this bitbucket repository, this notebook will do a VERY RUDIMENTARY search for license plates in the images. You can use this notebook as a starting point and extend it.

An example AWS lambda written in python is included in this bitbucket repository. This lambda can be used by your Lex chatbot to access your results JSON from S3.

The results of what you find should be stored in JSON format.

There is a simple web utility (mentioned above also) to explore the images and your JSON results. This web utility can be found here:

https://aiml-hackathon-ui.s3.eu-west-1.amazonaws.com/index.html

You can use any AWS Services that you need. Ask your instructor if you don't have access to any extra services.

